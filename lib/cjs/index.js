"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_js_1 = __importDefault(require("highlight.js/lib/core.js"));
var xml_1 = __importDefault(require("highlight.js/lib/languages/xml"));
var xml_parser_1 = __importDefault(require("xml-parser"));
var xml_render_1 = __importDefault(require("xml-render"));
var hyperscript_1 = __importDefault(require("hyperscript"));
core_js_1.default.configure({ languages: ['xml'] });
core_js_1.default.registerLanguage('xml', xml_1.default);
var Viewer = /** @class */ (function () {
    function Viewer(xml) {
        var _this = this;
        this.getElement = function () {
            return _this._el;
        };
        this.appendTo = function (el) {
            var self = _this;
            el.appendChild(_this._el);
            el.addEventListener('click', function (ev) {
                if (ev.target === el)
                    self._setSelection(null);
            });
        };
        this._setSelection = function (node) {
            if (_this._selection === node)
                return;
            if (_this._selection)
                _this._selection.el.classList.remove('selected');
            if (node)
                node.el.classList.add('selected');
            _this._selection = node;
        };
        this.getSelection = function () {
            return _this._selection;
        };
        this._renderRoot = function (node) {
            // const self = this;
            node.text = function () {
                return xml_render_1.default.node(node);
            };
            var declaration = (0, hyperscript_1.default)('div', {
                style: {
                    'margin-left': '20px'
                }
            }, xml_render_1.default.declaration(node.declaration));
            var el = (0, hyperscript_1.default)('div', declaration, _this._renderNode(node.root));
            core_js_1.default.highlightBlock(declaration);
            node.el = el;
            return el;
        };
        this._renderNode = function (node, i) {
            if (i === void 0) { i = 0; }
            var self = _this;
            // let folded = false;
            var indent = i;
            var left = indent * 20 + 20;
            if (!node.children || !node.children.length)
                return _this._renderLeaf(node, indent);
            var onToggle = function (ev) {
                ev.stopPropagation();
                if (node.folded) {
                    ev.target.innerHTML = '-';
                    node.children.forEach(function (child) {
                        child.el.style.display = 'block';
                    });
                    node.closeTag.style.display = 'block';
                }
                else {
                    ev.target.innerHTML = '+';
                    node.children.forEach(function (child) {
                        child.el.style.display = 'none';
                    });
                    node.closeTag.style.display = 'none';
                }
                node.folded = !node.folded;
            };
            node.text = function () {
                return xml_render_1.default.node(node);
            };
            var tagOpen = (0, hyperscript_1.default)('div', { style: { 'margin-left': "".concat(left, "px") } }, xml_render_1.default.tagOpen(node));
            var tagClose = (0, hyperscript_1.default)('div', { style: { 'margin-left': "".concat(left, "px") } }, xml_render_1.default.tagClose(node));
            var toggleWithTagOpen = (0, hyperscript_1.default)('div', { style: { display: 'flex', position: 'relative' } }, (0, hyperscript_1.default)('div', { onclick: onToggle, style: { color: 'red', width: '10px', cursor: 'pointer', left: "".concat(left - 20, "px"), position: 'absolute' } }, '-'), tagOpen);
            core_js_1.default.highlightBlock(tagOpen);
            var el = (0, hyperscript_1.default)('div', undefined, toggleWithTagOpen, node.children.map(function (child) {
                return self._renderNode(child, indent + 1);
            }), tagClose);
            core_js_1.default.highlightBlock(tagClose);
            node.el = el;
            node.closeTag = tagClose;
            node.folded = false;
            return el;
        };
        this._renderLeaf = function (node, indent) {
            node.text = function () {
                return xml_render_1.default.node(node);
            };
            var el = (0, hyperscript_1.default)('div', { style: { 'margin-left': "".concat(indent * 20 + 20, "px") } }, node.text());
            core_js_1.default.highlightBlock(el);
            node.el = el;
            return el;
        };
        xml = xml.toString();
        var obj = (0, xml_parser_1.default)(xml);
        this._el = this._renderRoot(obj);
        this._selection = null;
    }
    return Viewer;
}());
exports.default = Viewer;
