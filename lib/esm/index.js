import hljs from 'highlight.js/lib/core.js';
import xmlLang from 'highlight.js/lib/languages/xml';
import parse from 'xml-parser';
import render from 'xml-render';
import h from 'hyperscript';
hljs.configure({ languages: ['xml'] });
hljs.registerLanguage('xml', xmlLang);
var Viewer = /** @class */ (function () {
    function Viewer(xml) {
        var _this = this;
        this.getElement = function () {
            return _this._el;
        };
        this.appendTo = function (el) {
            var self = _this;
            el.appendChild(_this._el);
            el.addEventListener('click', function (ev) {
                if (ev.target === el)
                    self._setSelection(null);
            });
        };
        this._setSelection = function (node) {
            if (_this._selection === node)
                return;
            if (_this._selection)
                _this._selection.el.classList.remove('selected');
            if (node)
                node.el.classList.add('selected');
            _this._selection = node;
        };
        this.getSelection = function () {
            return _this._selection;
        };
        this._renderRoot = function (node) {
            // const self = this;
            node.text = function () {
                return render.node(node);
            };
            var declaration = h('div', {
                style: {
                    'margin-left': '20px'
                }
            }, render.declaration(node.declaration));
            var el = h('div', declaration, _this._renderNode(node.root));
            hljs.highlightBlock(declaration);
            node.el = el;
            return el;
        };
        this._renderNode = function (node, i) {
            if (i === void 0) { i = 0; }
            var self = _this;
            // let folded = false;
            var indent = i;
            var left = indent * 20 + 20;
            if (!node.children || !node.children.length)
                return _this._renderLeaf(node, indent);
            var onToggle = function (ev) {
                ev.stopPropagation();
                if (node.folded) {
                    ev.target.innerHTML = '-';
                    node.children.forEach(function (child) {
                        child.el.style.display = 'block';
                    });
                    node.closeTag.style.display = 'block';
                }
                else {
                    ev.target.innerHTML = '+';
                    node.children.forEach(function (child) {
                        child.el.style.display = 'none';
                    });
                    node.closeTag.style.display = 'none';
                }
                node.folded = !node.folded;
            };
            node.text = function () {
                return render.node(node);
            };
            var tagOpen = h('div', { style: { 'margin-left': "".concat(left, "px") } }, render.tagOpen(node));
            var tagClose = h('div', { style: { 'margin-left': "".concat(left, "px") } }, render.tagClose(node));
            var toggleWithTagOpen = h('div', { style: { display: 'flex', position: 'relative' } }, h('div', { onclick: onToggle, style: { color: 'red', width: '10px', cursor: 'pointer', left: "".concat(left - 20, "px"), position: 'absolute' } }, '-'), tagOpen);
            hljs.highlightBlock(tagOpen);
            var el = h('div', undefined, toggleWithTagOpen, node.children.map(function (child) {
                return self._renderNode(child, indent + 1);
            }), tagClose);
            hljs.highlightBlock(tagClose);
            node.el = el;
            node.closeTag = tagClose;
            node.folded = false;
            return el;
        };
        this._renderLeaf = function (node, indent) {
            node.text = function () {
                return render.node(node);
            };
            var el = h('div', { style: { 'margin-left': "".concat(indent * 20 + 20, "px") } }, node.text());
            hljs.highlightBlock(el);
            node.el = el;
            return el;
        };
        xml = xml.toString();
        var obj = parse(xml);
        this._el = this._renderRoot(obj);
        this._selection = null;
    }
    return Viewer;
}());
export default Viewer;
