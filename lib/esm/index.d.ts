import parse from 'xml-parser';
interface Node extends parse.Node {
    text?: () => string;
    el: HTMLElement;
    closeTag: HTMLElement;
    folded: boolean;
    children: Node[];
}
declare class Viewer {
    _el: HTMLElement;
    _selection: Node | null;
    constructor(xml: string);
    getElement: () => HTMLElement;
    appendTo: (el: HTMLElement) => void;
    private _setSelection;
    getSelection: () => Node | null;
    private _renderRoot;
    private _renderNode;
    private _renderLeaf;
}
export default Viewer;
