import hljs from 'highlight.js/lib/core.js';
import xmlLang from 'highlight.js/lib/languages/xml';
import parse from 'xml-parser';
import render from 'xml-render';
import h from 'hyperscript';

hljs.configure({languages: ['xml']});
hljs.registerLanguage('xml', xmlLang);

interface Document extends parse.Document {
    text?: () => string;
    el?: HTMLDivElement;
}
interface Node extends parse.Node {
    text?: () => string;
    el: HTMLElement;
    closeTag: HTMLElement;
    folded: boolean;
    children: Node[];
}

class Viewer{
    _el: HTMLElement;
    _selection: Node | null;
    constructor(xml: string) {
        xml = xml.toString();
        const obj = parse(xml);
        this._el = this._renderRoot(obj);
        this._selection = null;
    }
    public getElement = () => {
        return this._el;
    };
    public appendTo = (el: HTMLElement) => {
        const self = this;
        el.appendChild(this._el);
        el.addEventListener('click', function(ev){
            if (ev.target === el) self._setSelection(null);
        });
    };
    private _setSelection = (node: Node | null) => {
        if (this._selection === node) return;
        if (this._selection) this._selection.el.classList.remove('selected');
        if (node) node.el.classList.add('selected');
        this._selection = node;
    };
    public getSelection = () => {
        return this._selection;
    };
    private _renderRoot = (node: Document) => {
        // const self = this;
        node.text = function(){
            return render.node(node);
        };
        const declaration = h(
            'div',
            {
                style: {
                    'margin-left': '20px'
                }
            },
            render.declaration(node.declaration)
        );
        let el = h('div',
            declaration,
            this._renderNode(node.root as Node)
        );
        hljs.highlightBlock(declaration);
        node.el = el;
        return el;
    };
    private _renderNode = (node: Node, i = 0) => {
        const self = this;
        // let folded = false;
        let indent = i;
        const left = indent * 20 + 20;

        if (!node.children || !node.children.length) return this._renderLeaf(node, indent);

        const onToggle = (ev: MouseEvent) => {
            ev.stopPropagation();
            if (node.folded) {
                (<HTMLDivElement>ev.target).innerHTML = '-';
                node.children.forEach((child) => {
                    child.el.style.display = 'block';
                });
                node.closeTag.style.display = 'block';
            } else {
                (<HTMLDivElement>ev.target).innerHTML = '+';
                node.children.forEach((child) => {
                    child.el.style.display = 'none';
                });
                node.closeTag.style.display = 'none';
            }
            node.folded = !node.folded;
        }

        node.text = () => {
            return render.node(node);
        };
        const tagOpen = h('div',
            {style: {'margin-left': `${left}px`}},
            render.tagOpen(node)
        );
        const tagClose = h('div',
            {style: {'margin-left': `${left}px`}},
            render.tagClose(node)
        );
        const toggleWithTagOpen = h('div',
            {style: {display: 'flex', position: 'relative'}},
            h('div', { onclick: onToggle, style: {color: 'red', width: '10px', cursor: 'pointer', left: `${left - 20}px`, position: 'absolute'} }, '-'),
            tagOpen,
        );
        hljs.highlightBlock(tagOpen);
        const el: HTMLDivElement = h('div',
            undefined,
            toggleWithTagOpen,
            node.children.map((child) => {
                return self._renderNode(child, indent + 1);
            }),
            tagClose,
        );
        hljs.highlightBlock(tagClose);
        node.el = el;
        node.closeTag = tagClose;
        node.folded = false;

        return el;
    };
    private _renderLeaf = (node: Node, indent: number) => {
        node.text = () => {
            return render.node(node);
        };
        const el = h('div',
            {style: {'margin-left': `${indent * 20 + 20}px`}},
            node.text()
        );
        hljs.highlightBlock(el);
        node.el = el;

        return el;
    };
}

export default Viewer;
