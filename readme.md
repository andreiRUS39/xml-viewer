### XML-VIEWER

Библиотека для просмотра XML в стиле IE

*Usage*

**ES**

``` javascript
import Viewer from 'xml-viewer';
```

**CJS**

``` javascript
const Viewer = require('xml-viewer');
```

**Code**

``` javascript
const xml = 'xml_string';
const view = new Viewer(xml);
view.appendTo(HTML_ELEMENT)
// ...
<or>
// ...
HTML_ELEMENT.append(view.getElement())
```
